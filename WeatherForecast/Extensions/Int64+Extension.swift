//
//  String+Extension.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/15/22.
//

import Foundation

extension Int64 {
    
    func formatInt64ToStringDate() -> String {
        let timeinterval : TimeInterval = Double(self)
        let date = NSDate(timeIntervalSince1970:timeinterval)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "E, d MMM yyyy"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print("BaoVD ===> dateString \(dateString)")
        return dateString
    }
}
