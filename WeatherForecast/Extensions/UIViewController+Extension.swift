//
//  UIViewController+Extension.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/14/22.
//

import Foundation
import UIKit

extension UIViewController {

    func showNotification(title: String, message: String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
