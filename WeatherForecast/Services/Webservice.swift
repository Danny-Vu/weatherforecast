//
//  Webservice.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation

struct Resource<T> {
    let url: URL
    let parse: (Data) -> T?
}

enum APIFetchError: Error {
    case invalidUrl
    case unknown
}

final class Webservice {
    
    func load<T>(resource: Resource<T>, completion: @escaping (Result<T?,Error>) -> Void) {
        print("BaoVD ===> resource.url \(resource.url)")
        URLSession.shared.dataTask(with: resource.url) { data, response, error in
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                DispatchQueue.main.async {
                    print("BaoVD ===> completion(.success(resource.parse(data))) \(data)")
                    completion(.success(resource.parse(data)))
                }
            }else {
                completion(.failure(APIFetchError.unknown))
            }
        }.resume()
    }
}
