//
//  WeatherEntity.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation

class WeatherListViewModels: Equatable {
    static func == (lhs: WeatherListViewModels, rhs: WeatherListViewModels) -> Bool {
        return lhs.listWeather == rhs.listWeather &&
        lhs.listWeatherModels == rhs.listWeatherModels
    }
    
    let listWeather : WeatherResponse
    
    init(listWeather: WeatherResponse){
        self.listWeather = listWeather
    }
    
    var listWeatherModels: [WeatherViewModel] {
        var array : [WeatherViewModel] = []
        listWeather.list.forEach { weather in
            let item : WeatherViewModel = WeatherViewModel(weather: weather)
            array.append(item)
        }
        return array
    }
    
}

class WeatherViewModel: Equatable{
    
    static func == (lhs: WeatherViewModel, rhs: WeatherViewModel) -> Bool {
        return lhs.weather == rhs.weather &&
        lhs.dt == rhs.dt &&
        lhs.average == rhs.average &&
        lhs.description == rhs.description &&
        lhs.humidity == rhs.humidity &&
        lhs.pressure == rhs.pressure &&
        lhs.icon == rhs.icon
    }
    
    
    let weather: Weather
    
    init(weather: Weather) {
        self.weather = weather
    }
    
    var dt: String {
        
        return weather.dt.formatInt64ToStringDate()
    }
    
    var average: Int {
        return Int((weather.temp.max + weather.temp.min)/2)
    }
    
    var description: String{
        return weather.weather.first?.description ?? ""
    }
    
    var icon: String {
        return weather.weather.first?.icon ?? ""
    }
    
    var pressure: Int {
        return weather.pressure
    }
    
    var humidity: Int {
        return weather.humidity
    }
    
}
