//
//  AddWeatherViewModel.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation


class SearchWeatherViewModel {
    
    func searchCity(for city: String, complete: @escaping (Result<WeatherListViewModels,Error>) -> Void) {
        guard let weatherURL = Constants.Urls.urlByCity(city: city) else {
            return complete(.failure(APIFetchError.invalidUrl))
        }
        
        let weatherResource = Resource<WeatherResponse>(url: weatherURL) { data in
            let weatherResponse = try? JSONDecoder().decode(WeatherResponse.self, from: data)
            print("BaoVD ===> weatherResponse \(String(describing: weatherResponse))")
            return weatherResponse
        }
        
        Webservice().load(resource: weatherResource) { results in
            switch results {
            case .success(let weather):
                let weather = WeatherListViewModels(listWeather: weather ?? WeatherResponse(list: []))
                print("BaoVD ===> complete(.success(weather))\(weather)")
                complete(.success(weather))
            case .failure(let error):
                complete(.failure(error))
            }
        }
    }
    
    func fetchIcon(for iconName: String, complete: @escaping (Result<Data,Error>) -> Void) {
        guard let iconURL = Constants.Urls.urlByIcon(iconName: iconName) else {
            return complete(.failure(APIFetchError.invalidUrl))
        }
        
        let dataResource = Resource<Data>(url: iconURL) { data in
            
            return data
        }
        
        Webservice().load(resource: dataResource) { results in
            switch results {
            case .success(let data):
                complete(.success(data!))
            case .failure(let error):
                complete(.failure(error))
            }
        }
        
    }
}
