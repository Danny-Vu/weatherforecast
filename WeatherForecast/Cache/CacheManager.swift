//
//  CacheManager.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation
import UIKit

class CacheManager {
    static let shared = CacheManager()
    
    let weatherCache: NSCache<NSString, CacheModel> = {
        let cache = NSCache<NSString, CacheModel>()
        cache.name = "WeatherForecast"
        return cache
    }()
    
    
    // get
    func getCache( key: String) -> CacheModel? {
        if let cachedData = weatherCache.object(forKey: key as NSString) {
            return cachedData
        }
        return nil
    }
    
    // set
    func setCache( key: String,  data: CacheModel) {
        weatherCache.setObject(data, forKey: key as NSString)
    }
}
