//
//  CacheModel.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/15/22.
//

import Foundation
import UIKit

class CacheModel {
    var timestamp: TimeInterval
    var listImage: [UIImage] = []
    var listData: [WeatherViewModel] = []
    
    init(timestamp: TimeInterval, images: [UIImage], datas: [WeatherViewModel]) {
        self.timestamp = timestamp
        self.listImage = images
        self.listData = datas
    }
}
