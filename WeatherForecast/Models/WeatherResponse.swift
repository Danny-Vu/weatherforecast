//
//  WeatherResponse.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation

struct WeatherResponse: Codable, Equatable {
    static func == (lhs: WeatherResponse, rhs: WeatherResponse) -> Bool {
        return lhs.list == rhs.list
    }
    
    let list: [Weather]
}

struct Weather: Codable, Equatable {
    static func == (lhs: Weather, rhs: Weather) -> Bool {
        return
        lhs.dt == rhs.dt &&
        lhs.weather == rhs.weather &&
        lhs.humidity == rhs.humidity &&
        lhs.pressure == rhs.pressure &&
        lhs.temp == rhs.temp
    }
    
    let humidity: Int
    let pressure: Int
    let dt: Int64
    let temp: Templeture
    let weather: [Description]
    
}

struct Templeture: Codable , Equatable{
    let min: Double
    let max: Double
}

struct Description: Codable , Equatable {
    let description: String
    let icon: String
}
