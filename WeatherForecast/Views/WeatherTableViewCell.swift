//
//  WeatherTableViewCell.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    static let identifier = "WeatherTableViewCell"
    
    // MARK: VARIABLE
    var searchWeatherViewModel = SearchWeatherViewModel()
    lazy var lab : UILabel = {
        let lab = UILabel()
        lab.font = UIFont.preferredFont(forTextStyle: .body)
        lab.translatesAutoresizingMaskIntoConstraints = false
        lab.numberOfLines = 0
        lab.textColor = .black
        
        return lab
    }()
    
    private let heroImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let line: UIView = {
        let line = UIView()
        line.backgroundColor = .lightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor(displayP3Red: 225/255, green: 221/255, blue: 216/255, alpha: 1.0)
        applyConstraints()

    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    // MARK: LAYOUT
    private func applyConstraints(){
        addSubview(lab)
        addSubview(heroImageView)
        addSubview(line)
        
        let lblConstraints = [
            lab.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            lab.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            lab.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            lab.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -100),
            
        ]
        
        let imgConstraints = [
            heroImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            heroImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            heroImageView.heightAnchor.constraint(equalToConstant: 70),
            heroImageView.widthAnchor.constraint(equalToConstant: 70),
            
        ]
        
        let lineConstraints = [
            line.topAnchor.constraint(equalTo: lab.bottomAnchor, constant: 10),
            line.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            line.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            line.heightAnchor.constraint(equalToConstant: 1),
        ]
        
        NSLayoutConstraint.activate(lblConstraints)
        NSLayoutConstraint.activate(imgConstraints)
        NSLayoutConstraint.activate(lineConstraints)
        
        lab.isAccessibilityElement = true
        lab.accessibilityHint = "This text will talk about the weather"
    
    }
    
    // MARK: HANDLE Cell
    public func configure(with weatherViewModel: WeatherViewModel, uiImage: UIImage?){

        lab.text = "Date: \(weatherViewModel.dt)\nAverage Temperature: \(weatherViewModel.average)°C\nPressure: \(weatherViewModel.pressure)\nHumidity: \(weatherViewModel.humidity)%\nDescription: \(weatherViewModel.description)"
        lab.accessibilityValue = "The date is \(weatherViewModel.dt), average temperature is \(weatherViewModel.average) degree, humidity is \(weatherViewModel.humidity) and the weather is \(weatherViewModel.description)"
        
        if(uiImage != nil){
            self.heroImageView.image = uiImage
        }else{
            searchWeatherViewModel.fetchIcon(for: weatherViewModel.icon) { [weak self] results in
                switch results {
                case .success(let iconData):
                    DispatchQueue.main.async {
                        self?.heroImageView.image = UIImage(data: iconData)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    
}
