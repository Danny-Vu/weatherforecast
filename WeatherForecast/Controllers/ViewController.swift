//
//  ViewController.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: VARIABLE
    private let tblView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    private let navBar: UIView = {
        let navigationBar = UIView()
        navigationBar.backgroundColor = .white
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        return navigationBar
    }()
    
    private let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "Weather Forecast"
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.barTintColor = UIColor.white
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Search and enter...."
        searchBar.searchBarStyle = .minimal
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.backgroundColor = .gray
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    var searchWeatherViewModel = SearchWeatherViewModel()
    let searchController = UISearchController()
    let screenSize: CGRect = UIScreen.main.bounds
    var listData: [WeatherViewModel] = []
    var text = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setTableView()
        self.setSpinnerView()
        
        
    }
    
    // MARK: LAYOUT
    
    func setNavigationBar() {
        self.view.addSubview(navBar)
        searchBar.delegate = self
        navBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        navBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        navBar.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        navBar.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        self.navBar.addSubview(lblTitle)
        lblTitle.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        lblTitle.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 40).isActive = true
        lblTitle.widthAnchor.constraint(equalToConstant: 200).isActive = true
        lblTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.navBar.addSubview(searchBar)
        searchBar.leadingAnchor.constraint(equalTo: navBar.leadingAnchor, constant: 0).isActive = true
        searchBar.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 80).isActive = true
        searchBar.widthAnchor.constraint(equalToConstant: view.frame.size.width).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    private func setTableView(){
        view.addSubview(tblView)
        tblView.dataSource = self
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 300
        tblView.register(WeatherTableViewCell.self, forCellReuseIdentifier: WeatherTableViewCell.identifier)
        tblView.separatorColor = .clear
        tblView.backgroundColor = .lightGray
        
        tblView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        tblView.topAnchor.constraint(equalTo: view.topAnchor, constant: 130).isActive = true
        tblView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tblView.heightAnchor.constraint(equalToConstant: view.bounds.height - 130).isActive = true
    }
    
    private func setSpinnerView(){
        view.addSubview(spinner)
        spinner.centerXAnchor.constraint(equalTo: tblView.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: tblView.centerYAnchor).isActive = true
        spinner.widthAnchor.constraint(equalToConstant: 50).isActive = true
        spinner.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
}

// MARK: TABLEVIEW
extension ViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableViewCell.identifier, for: indexPath) as? WeatherTableViewCell else{
            return UITableViewCell()
        }
        
        if let cacheData = CacheManager.shared.getCache(key: self.text) {
            
            if NSDate().timeIntervalSince1970 - cacheData.timestamp > 86400 {
                cell.configure(with: self.listData[indexPath.row], uiImage: nil)
            } else {
                cell.configure(with: self.listData[indexPath.row], uiImage: cacheData.listImage[indexPath.row])
            }
        } else {
            callSearchApi(text: text.lowercased())
        }
        
        
        return cell
    }
    
}

// MARK: SEARCH CONTROLLER
extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count > 3){
            self.spinner.startAnimating()
        }else{
            self.listData = []
            self.spinner.stopAnimating()
            self.tblView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.listData = []
        self.spinner.stopAnimating()
        self.tblView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else{
            return
        }
        if (text.count > 3){
            if let cacheData = CacheManager.shared.getCache(key: text.lowercased()) {
                
                if NSDate().timeIntervalSince1970 - cacheData.timestamp > 86400 {
                    callSearchApi(text: text.lowercased())
                } else {
                    self.spinner.stopAnimating()
                    self.listData = cacheData.listData
                    self.tblView.reloadData()
                }
                
            } else {
                callSearchApi(text: text.lowercased())
            }
        }
    }
    
    func callSearchApi(text: String) {
        self.searchWeatherViewModel.searchCity(for: text) { [weak self] results in
            switch results {
            case .success(let weather):
                self?.spinner.stopAnimating()
                guard weather.listWeather.list.count > 0 else {
                    self?.showNotification(title: "NOTICE", message: "Can not find any city's weather now. Please search again.")
                    return
                }
                
                self?.listData = weather.listWeatherModels
                self?.convertFromListIconsToListImages(listData: self?.listData ?? [], complete: { listImages in
                    // save cache
                    self?.text = text
                    CacheManager.shared.setCache(key: text, data: CacheModel(timestamp: NSDate().timeIntervalSince1970, images: listImages, datas: weather.listWeatherModels))
                    self?.tblView.reloadData()
                })
                
            case .failure(let error):
                self?.showNotification(title: "NOTICE", message: "\(error)")
            }
            
        }
    }
    
    func convertFromListIconsToListImages(listData: [WeatherViewModel], complete: @escaping ([UIImage]) -> Void){
        
        var listImages: [UIImage] = []
        listData.forEach({ item in
            self.searchWeatherViewModel.fetchIcon(for: item.icon) { [weak self] results in
                switch results {
                case .success(let data):
                    listImages.append(UIImage(data: data)!)
                    if(listData.count == listImages.count){
                        complete(listImages)
                    }
                case .failure(_):
                    self?.showNotification(title: "NOTICE", message: "It has an error when caching the image")
                }
                
            }
        })
    }
    
}


