//
//  Constant.swift
//  WeatherForecast
//
//  Created by Bao Vu on 7/12/22.
//

import Foundation

struct Constants {
    struct Urls {
        static func urlByCity(city: String) -> URL? {
            let parseCity = city.replacingOccurrences(of: "\"", with: "")
            guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast/daily?q=\(parseCity)&cnt=7&appid=60c6fbeb4b93ac653c492ba806fc346d&units=metric") else { return nil
            }
            return url
        }
        
        static func urlByIcon(iconName: String) -> URL? {
            let icon = iconName.replacingOccurrences(of: "\"", with: "")
            guard let url = URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png") else { return nil }
            return url
        }
    }
}
