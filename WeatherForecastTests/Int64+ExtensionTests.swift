//
//  Int64+ExtensionTests.swift
//  WeatherForecastTests
//
//  Created by Bao Vu on 7/15/22.
//

import XCTest
@testable import WeatherForecast

class Int64_ExtensionTests: XCTestCase {

    var sut : Int64!
    
    override func setUpWithError() throws {
        sut = Int64()
    }

    func testInt64ShouldFormatToDateString() {
        sut = 1657857600
        let result: String = sut.formatInt64ToStringDate()
        XCTAssertEqual(result, "Fri, 15 Jul 2022")
    }
    
    override func tearDownWithError() throws {
        sut = nil
    }


}
