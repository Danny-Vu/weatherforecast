//
//  WeatherListViewModelsTests.swift
//  WeatherForecastTests
//
//  Created by Bao Vu on 7/15/22.
//

import XCTest
@testable import WeatherForecast

class WeatherListViewModelsTests: XCTestCase {

    var sut : WeatherListViewModels!
    
    override func setUpWithError() throws {
        sut = WeatherListViewModels(listWeather: WeatherResponse(list: [
        Weather(humidity: 1, pressure: 2, dt: 123, temp: Templeture(min: 20, max: 30), weather: [Description(description: "cloud", icon: "id10")]),
        Weather(humidity: 2, pressure: 3, dt: 124, temp: Templeture(min: 25, max: 35), weather: [Description(description: "rain", icon: "id11")])
       ]))
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testWeatherResponseShouldConvertToWetherListViewModels() {
        
        let rightData = [
            WeatherViewModel(weather: Weather(humidity: 1, pressure: 2, dt: 123, temp: Templeture(min: 20, max: 30), weather: [Description(description: "cloud", icon: "id10")])),
            WeatherViewModel(weather: Weather(humidity: 2, pressure: 3, dt: 124, temp: Templeture(min: 25, max: 35), weather: [Description(description: "rain", icon: "id11")])),
            ]
    
        XCTAssertEqual(sut.listWeatherModels, rightData)
    }


}
